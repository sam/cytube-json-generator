import { assertEquals } from "https://deno.land/std@0.141.0/testing/asserts.ts";
//Create map of video files and subtitles for testing
const files = new Map<string, string>();
files.set(
  "8.Mile.2002.720p.BluRay.999MB.HQ.x265.10bit.mp4",
  "8.Mile.2002.720p.BluRay.999MB.HQ.x265.10bit.srt",
);
files.set("Blade.Runner (1997).mp4", "Blade.Runner (1997).srt");
files.set(
  "Total.Recall.1990.REMASTERED.720p.BluRay.999MB.HQ.x265.mp4",
  "Total.Recall.1990.REMASTERED.720p.BluRay.999MB.HQ.x265.srt",
);
files.set("", "");
files.set(
  "[Biraru] Chi's Sweet Home - 022 [DVD][576p][10-bit][x264][AC3][3CBD659E].mkv",
  "[Biraru] Chi's Sweet Home - 022 [DVD][576p][10-bit][x264][AC3][3CBD659E].srt",
);
files.set(
  "[Reaktor] Legend of the Galactic Heroes - Ginga Eiyuu Densetsu - Gaiden - Arc 1 - E1 [720p][x265][10-bit].mp4",
  "[Reaktor] Legend of the Galactic Heroes - Ginga Eiyuu Densetsu - Gaiden - Arc 1 - E1 [720p][x265][10-bit].srt",
);

Deno.test({
  name: "File parsing test",
  fn: () => {
    files.forEach((file) => {
      const videoName = file
        .split("/")
        .pop()!
        .slice(0, file.length - 4) as string;
      assertEquals(videoName, file.slice(0, file.length - 4));
    });
  },
});
